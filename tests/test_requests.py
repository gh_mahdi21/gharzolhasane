from django.test import TestCase, Client
from django.test.client import RequestFactory
from login.views import Login
from book.views import Home, AddBook, Change, Invite
from django.contrib.auth.models import User, AnonymousUser
from django.urls import reverse


class RequestTests(TestCase):
    def setUp(self):
        # Every test needs access to the request factory.
        self.factory = RequestFactory()
        self.user = User.objects.create_user(
            username='mahdi', email='gh_mahdi21@yahoo.com', password='top_secret')
        self.anonymous = AnonymousUser()

    def test_login_view(self):
        request = self.factory.get('/')
        request.user = self.anonymous
        response = Login.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "ورود")

    def test_login_redirection(self):
        request = self.factory.get('/')
        request.user = self.user
        response = Login.as_view()(request)
        self.assertEqual(response.status_code, 302)

    def test_home_view(self):
        request = self.factory.get('/')
        request.user = self.user
        response = Home.as_view()(request)
        self.assertEqual(response.status_code, 200)

    def test_add_view(self):
        request = self.factory.get('/')
        request.user = self.user
        response = AddBook.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "اضافه کردن کتاب")

    def test_change_view(self):
        request = self.factory.get('/')
        request.user = self.user
        response = Change.as_view()(request)
        self.assertEqual(response.status_code, 200)

    def test_invite(self):
        client = Client()
        User.objects.create_user(
            username='mahdii', email='gh_mahdi22@yahoo.com', password='top_secret')
        client.login(username='mahdi', password='top_secret')
        response = client.generic('POST', '/book/invite/', '{"email":"mahdi.ghorbani.21@gmail.com"}')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "لینک ثبت نام برای ایمیل وارد شده ارسال شد")
        self.assertContains(response, "/signup?signuphash")
