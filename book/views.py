from django.shortcuts import render
from .models import Book, Invitation
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django_user_agents.utils import get_user_agent
import json
from django.views import View
from django.core.mail import EmailMessage
from django.utils.crypto import get_random_string

inviteMessage=' شما را به سرویس اشتراک کتاب دعوت کرده است، برای ثبت نام روی لینک زیر کلیک کنید و در قسمت کد معرف کد زیر را وارد کنید :‌ '
emailSubject = 'کتاب یاب'

def make_data_ready(request):
    user_agent = get_user_agent(request)
    if user_agent.is_pc:
        post = request.POST
    else:
        post = json.loads(request.body)
    return post


def make_response(request, web_html, web_data, mobile_json, authentication_required=False):
    user_agent = get_user_agent(request)
    if not user_agent.is_pc:
        return JsonResponse(mobile_json)
    if authentication_required and not request.user.is_authenticated:
        return HttpResponseRedirect('/login')
    return render(request, web_html, web_data)


class Home(View):
    def get(self, request):
        return make_response(request, 'main.html', {'user': request.user, 'books': Book.objects.all()},
                            {'books': list(Book.objects.all().values())}, True)


class AddBook(View):
    def post(self, request):
        post = make_data_ready(request)
        user_agent = get_user_agent(request)
        if 'bookID' in post.keys():
            book = Book.objects.get(id=post['bookID'])
            book.image = request.FILES['file']
            book.save()
            if not user_agent.is_pc:
                return JsonResponse({'response': 'ok'})
            return render(request, 'addBook.html', {'success': True})
        book = Book.objects.create(user=request.user, name=post['name'], author=post['author'], genre=post['genre'],
                                   age=post['age'], pubyear=post['pubyear'], description=post['description'],
                                   dimension=post['dimension'], summary=post['summary'], tags=post['tags'])
        if not user_agent.is_pc:
            return JsonResponse({'response': 'ok'})
        return render(request, 'addBook.html', {'bookID': book.id})

    def get(self, request):
        return render(request, 'addBook.html')


class Change(View):
    def get(self, request):
        return make_response(request, 'removeBook.html', {'books': Book.objects.filter(user=request.user)},
                     {'books': list(Book.objects.filter(user=request.user).values())})


class RemoveBook(View):
    def post(self, request):
        post = make_data_ready(request)
        deleteID = post['deleteID']
        Book.objects.get(id=deleteID).delete()
        return JsonResponse({'done': 'done'})


class EditBook(View):
    def post(self, request):
        post = make_data_ready(request)
        user_agent = get_user_agent(request)
        editID = post['editID']
        book = Book.objects.get(id=editID)
        if user_agent.is_pc:
            setattr(book, post['name'], post['value'])
        else:
            for name in post.keys():
                if name != 'editID':
                    setattr(book, name, post[name])
        book.save()
        return JsonResponse({'done': 'done'})


class Detail(View):
    def get(self, request):
        return render(request, 'detail.html', {'book': Book.objects.get(id=request.GET['id'])})


class Invite(View):
    def get(self, request):
        return render(request, 'invite.html')

    def post(self, request):
        user_agent = get_user_agent(request)
        post = make_data_ready(request)
        email = post['email']
        hash_string = get_random_string(length=6)
        Invitation.objects.create(inviter=request.user, invited_email=email, hash_string=hash_string)
        link = request.build_absolute_uri()[:-12] + 'login'
        email_sender = EmailMessage(emailSubject,
                                    request.user.first_name +inviteMessage+ '\n'
                                    + hash_string +'\n'+link
                                    , to=[email])
        try:
            email_sender.send()
        except:
            return render(request, 'invite.html', {'error': True})
        if not user_agent.is_pc:
            return JsonResponse({'invited': True})
        return render(request, 'invite.html', {'success': True, 'link': link})
