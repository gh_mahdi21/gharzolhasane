from django.urls import path

from . import views

urlpatterns = [
    path('', views.Home.as_view(), name='home'),
    path('add/', views.AddBook.as_view(), name='addBook'),
    path('remove/', views.RemoveBook.as_view(), name='removeBook'),
    path('edit/', views.EditBook.as_view(), name='editBook'),
    path('change/', views.Change.as_view(), name='changeBook'),
    path('detail/', views.Detail.as_view(), name='detail'),
    path('invite/', views.Invite.as_view(), name='invite'),

]
