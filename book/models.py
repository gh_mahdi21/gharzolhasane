from django.db import models
from django.contrib.auth.models import User
import uuid


def user_directory_path(instance, filename):
    return 'user_{0}/'.format(instance.user.id) + str(uuid.uuid4()) + '.jpg'


class Book(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=33)
    author = models.CharField(max_length=50)
    genre = models.CharField(max_length=50)
    age = models.CharField(max_length=50)
    pubyear = models.CharField(max_length=50)
    dimension = models.CharField(max_length=50)
    description = models.CharField(max_length=50)
    summary = models.CharField(max_length=50)
    tags = models.CharField(max_length=50)
    image = models.ImageField(upload_to=user_directory_path, null=True)
    availible = models.BooleanField(default=True)


class Borrowing(models.Model):
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    borrower = models.ForeignKey(User, on_delete=models.CASCADE, related_name='Borrowing_borrower')
    bailer = models.ForeignKey(User, on_delete=models.CASCADE, related_name='Borrowing_bailer')
    start_date = models.DateField()
    duration = models.IntegerField()
    cost = models.IntegerField()
    end_date = models.DateField()
    accepted = models.BooleanField(default=False)


class Invitation(models.Model):
    hash_string = models.CharField(max_length=33)
    inviter = models.ForeignKey(User, on_delete=models.CASCADE)
    invited_email = models.CharField(max_length=50)