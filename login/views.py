from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django_user_agents.utils import get_user_agent
from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import render
from .models import Profile
from django.contrib.auth.models import User
from django.core.mail import EmailMessage
from django.utils.crypto import get_random_string
from django.views import View
from book.models import Invitation
import json, random, string
from kavenegar import *

APIkey = '477056774161544271634B6F663668547855737532384E4B3638314F5A385378'
smsMessage = 'به کتاب یاب خوش آمدید. کد شما : '
emailSubject = 'کتاب یاب'
resetPasswordMessage = 'برای تغییر رمز عبور خود بر روی لینک کلیک کنید : '


def make_data_ready(request):
    user_agent = get_user_agent(request)
    if user_agent.is_pc:
        post = request.POST
    else:
        post = json.loads(request.body)
    return post


def sendSMS(receptor, message):
    try:
        api = KavenegarAPI(APIkey)
        params = {
            'receptor': receptor,
            'message': message,
        }
        response = api.sms_send(params)
        print(response)
    except APIException as e:
        print(e)
    except HTTPException as e:
        print(e)


class Logout(View):
    def get(self, request):
        auth_logout(request)
        return HttpResponseRedirect('/login')

    def post(self, request):
        auth_logout(request)
        return JsonResponse({'response': 'logedOut'})


class Login(View):
    def post(self, request):
        user_agent = get_user_agent(request)
        post = make_data_ready(request)
        if 'cell' in post:
            key = ''.join(random.choices(string.ascii_uppercase + string.digits, k=6))
            try:
                user = User.objects.get(username=post['cell'])
                user.set_password(key)
                user.save()
            except:
                User.objects.create_user(username=post['cell'], password=key)
            sendSMS(post['cell'], smsMessage + key)
            if user_agent.is_pc:
                return HttpResponseRedirect('/login?verify=' + post['cell'])
            else:
                return JsonResponse({'response': 'ok'})
        if 'key' in post:
            user = authenticate(request, username=post['cellKey'], password=post['key'])
            if user is not None:
                auth_login(request, user)
                if request.user.is_authenticated:
                    if user_agent.is_pc:
                        if user.first_name:
                            return HttpResponseRedirect('/book')
                        return render(request, 'signUp.html', {'cell': post['cellKey'], 'key': post['key']})
                    else:
                        if user.first_name:
                            return JsonResponse({'response': 'ok'})
                        return JsonResponse({'response': 'signup'})
                else:
                    if user_agent.is_pc:
                        return HttpResponseRedirect('/login?verify=' + post['cellKey'] + '&err=True')
                    else:
                        return JsonResponse({'response': 'no'})
            else:
                return HttpResponseRedirect('/login?verify=' + post['cellKey'] + '&err=True')

    def get(self, request):
        if request.user.is_authenticated:
            return HttpResponseRedirect('/book')
        err = verify = False
        if 'verify' in request.GET:
            verify = request.GET['verify']
        if 'err' in request.GET:
            err = request.GET['err']
        return render(request, 'login.html', {'verify': verify, 'err': err})


class Signup(View):
    def post(self, request):
        post = make_data_ready(request)
        try:
            user = authenticate(request, username=post['cell'], password=post['key'])
            if user is None:
                return HttpResponseRedirect('/login')
        except:
            return HttpResponseRedirect('/login')
        try:
            user.first_name = post['name']
            user.email = post['email']
            user.save()
        except:
            return render(request, 'signUp.html', {'cell': post['cell'], 'key': post['key'], 'err': True})
        if 'inviter' in post and post['inviter']:
            try:
                invitation = Invitation.objects.get(hash_string=post['inviter'])
            except:
                return render(request, 'signUp.html', {'cell': post['cell'], 'key': post['key'], 'err': True})
            try:
                profile = Profile.objects.get(user=user)
                profile.inviter = invitation.inviter
                profile.save()
            except:
                Profile.objects.create(user=user, inviter=invitation.inviter)

        return HttpResponseRedirect('/book')

    def get(self, request):
        return render(request, 'signUp.html', {'cell': '09369418892', 'key': 'DIANAH'})