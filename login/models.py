from django.db import models

from django.contrib.auth.models import User


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='Profile_user')
    address = models.CharField(max_length=500)
    inviter = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name='Profile_inviter')

    def __str__(self):
        return self.user.username